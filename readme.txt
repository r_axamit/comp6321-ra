Directory structure:
report – directory contains a full report file - report.pdf and TEX sources.
src – directory with source code. Files/directories are arranged in accordance with type of experiment, e.g. “classification”

Section “Overview of project code and data” of report.pdf provides instructions on executing experiment scripts (command-line syntax).

There are additional resources that exceed size limit and as such should be downloaded separately – please use following links to get these resources.
Data files: https://drive.google.com/open?id=18OBQVRFbXjAQpM3_K15N9bOMfIHPpSiN
Model files: https://drive.google.com/open?id=1H72NbouunrQ81j4klosV4btsk4TUPXl_