#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import sklearn
import sklearn.tree
import torch
import sys
import os
import getopt
import enum
import math
import pickle


CIFAR_PATH={'train': ['data_batch_1', 'data_batch_2', 'data_batch_3', 'data_batch_4', 'data_batch_5'], 'test': ['test_batch']}

#**************************************************************************************************
#Cifar data
#--------------------------------------------------------------------------------------------------
class CifarData:
    def __init__(self, aPath):
        self.mDataPath=aPath
        
    def GetRawData(self, aDataType):
        lData=None
        lLabels=None
        for lFilePath in self.mDataPath[aDataType]:
            with open(lFilePath, 'rb') as lhFile:
                lDict=pickle.load(lhFile, encoding='bytes')
                if lData is None and lLabels is None:
                    lData=np.array(lDict[b'data'], dtype=np.uint8)
                    lLabels=np.array(lDict[b'labels'], dtype=np.int32)
                else:
                    lData=np.vstack((lData, np.array(lDict[b'data'], dtype=np.uint8)))
                    lLabels=np.hstack((lLabels, np.array(lDict[b'labels'], dtype=np.int64)))
        return (lData, lLabels)

    def GetImgData(self, aDataType):
        lRawX, lRawY=self.GetRawData(aDataType)
        lImgX=lRawX.reshape(-1, 3, 32, 32).transpose(0, 2, 3, 1)
        return (lImgX, lRawY)
    
    def GetScaledData(self, aDataType):
        lRawX, lRawY=self.GetRawData(aDataType)
        lSclX=lRawX.reshape(-1, 3, 32, 32)
        lSclX=np.interp(lSclX, (0, 255), (-1, +1)).astype(np.float32)
        return (torch.from_numpy(lSclX), torch.from_numpy(lRawY))
    
    def GetNonsense(self):
        lImg=np.full((1, 32, 32, 3), 127, dtype=np.uint8)
        lTorch=torch.full((1, 3, 32, 32), 0, dtype=torch.float32, requires_grad=True)
        return (lImg, lTorch)
                            
#==================================================================================================


#**************************************************************************************************
#CvNet
#--------------------------------------------------------------------------------------------------
class CvNet(torch.nn.Module):
    def __init__(self):
        super(CvNet, self).__init__()
        self.__mShowOnce=False
        self.__CONV1_INPUT_SIZE=32
        self.__CONV1_OUTPUT_SIZE=self.__CONV1_INPUT_SIZE
        self.__CONV1_INPUT_CH=3
        self.__CONV1_KERNEL_SIZE=3
        self.__CONV1_FILTER_NUM=16
        self.__CONV1_STRIDE=1
        self.__CONV1_PADDING=int(math.ceil(((self.__CONV1_STRIDE*(self.__CONV1_OUTPUT_SIZE-1))-self.__CONV1_INPUT_SIZE+self.__CONV1_KERNEL_SIZE)/2))
        self.__CONV2_INPUT_SIZE=self.__CONV1_OUTPUT_SIZE
        self.__CONV2_OUTPUT_SIZE=self.__CONV2_INPUT_SIZE
        self.__CONV2_INPUT_CH=self.__CONV1_FILTER_NUM        
        self.__CONV2_KERNEL_SIZE=3
        self.__CONV2_FILTER_NUM=32
        self.__CONV2_STRIDE=1
        self.__CONV2_PADDING=int(math.ceil(((self.__CONV2_STRIDE*(self.__CONV2_OUTPUT_SIZE-1))-self.__CONV2_INPUT_SIZE+self.__CONV2_KERNEL_SIZE)/2))
        self.__POOL1_KERNEL_SIZE=2
        self.__POOL1_KERNEL_OUTPUT_SIZE=self.__CONV2_OUTPUT_SIZE//self.__POOL1_KERNEL_SIZE
        self.__CONV3_INPUT_SIZE=self.__POOL1_KERNEL_OUTPUT_SIZE
        self.__CONV3_OUTPUT_SIZE=self.__CONV3_INPUT_SIZE
        self.__CONV3_INPUT_CH=self.__CONV2_FILTER_NUM        
        self.__CONV3_KERNEL_SIZE=3
        self.__CONV3_FILTER_NUM=64
        self.__CONV3_STRIDE=1
        self.__CONV3_PADDING=int(math.ceil(((self.__CONV3_STRIDE*(self.__CONV3_OUTPUT_SIZE-1))-self.__CONV3_INPUT_SIZE+self.__CONV3_KERNEL_SIZE)/2))
        self.__CONV4_INPUT_SIZE=self.__CONV3_OUTPUT_SIZE
        self.__CONV4_OUTPUT_SIZE=self.__CONV4_INPUT_SIZE
        self.__CONV4_INPUT_CH=self.__CONV3_FILTER_NUM        
        self.__CONV4_KERNEL_SIZE=3
        self.__CONV4_FILTER_NUM=128
        self.__CONV4_STRIDE=1
        self.__CONV4_PADDING=int(math.ceil(((self.__CONV4_STRIDE*(self.__CONV4_OUTPUT_SIZE-1))-self.__CONV4_INPUT_SIZE+self.__CONV4_KERNEL_SIZE)/2))
        self.__POOL2_KERNEL_SIZE=2
        self.__POOL2_KERNEL_OUTPUT_SIZE=self.__CONV4_OUTPUT_SIZE//self.__POOL2_KERNEL_SIZE
        self.__LIN1_INPUT_SIZE=self.__CONV4_FILTER_NUM*self.__POOL2_KERNEL_OUTPUT_SIZE*self.__POOL2_KERNEL_OUTPUT_SIZE
        self.__LIN1_OUTPUT_SIZE=4096
        self.__LIN2_INPUT_SIZE=self.__LIN1_OUTPUT_SIZE
        self.__LIN2_OUTPUT_SIZE=1024
        self.__LIN3_INPUT_SIZE= self.__LIN2_OUTPUT_SIZE
        self.__LIN3_OUTPUT_SIZE=10
        
        print('Conv1 padding={:}'.format(self.__CONV1_PADDING))
        self.mConv1=torch.nn.Conv2d(self.__CONV1_INPUT_CH, self.__CONV1_FILTER_NUM, self.__CONV1_KERNEL_SIZE, stride=self.__CONV1_STRIDE, padding=self.__CONV1_PADDING)
        
        print('Conv2 padding={:}'.format(self.__CONV2_PADDING))
        self.mConv2=torch.nn.Conv2d(self.__CONV2_INPUT_CH, self.__CONV2_FILTER_NUM, self.__CONV2_KERNEL_SIZE, stride=self.__CONV2_STRIDE, padding=self.__CONV2_PADDING)
        
        print('Pool1 output size={:}'.format(self.__POOL1_KERNEL_OUTPUT_SIZE))
        
        print('Conv3 padding={:}'.format(self.__CONV3_PADDING))
        self.mConv3=torch.nn.Conv2d(self.__CONV3_INPUT_CH, self.__CONV3_FILTER_NUM, self.__CONV3_KERNEL_SIZE, stride=self.__CONV3_STRIDE, padding=self.__CONV3_PADDING)
        
        print('Conv4 padding={:}'.format(self.__CONV4_PADDING))
        self.mConv4=torch.nn.Conv2d(self.__CONV4_INPUT_CH, self.__CONV4_FILTER_NUM, self.__CONV4_KERNEL_SIZE, stride=self.__CONV4_STRIDE, padding=self.__CONV4_PADDING)
        
        print('Pool2 output size={:}'.format(self.__POOL2_KERNEL_OUTPUT_SIZE))
        
        self.mPool1=torch.nn.MaxPool2d(self.__POOL1_KERNEL_SIZE)
        self.mPool2=torch.nn.MaxPool2d(self.__POOL2_KERNEL_SIZE)
        
        print('Lin1 input size={:}'.format(self.__LIN1_INPUT_SIZE))
        self.mLin1=torch.nn.Linear(self.__LIN1_INPUT_SIZE, self.__LIN1_OUTPUT_SIZE)
        self.mLin2=torch.nn.Linear(self.__LIN2_INPUT_SIZE, self.__LIN2_OUTPUT_SIZE)
        self.mLin3=torch.nn.Linear(self.__LIN3_INPUT_SIZE, self.__LIN3_OUTPUT_SIZE)

    def forward(self, aX):        
        lResult=self.mConv1(aX)
        if self.__mShowOnce==False:
            print('Conv1: {:}'.format(lResult.shape))
        lResult=torch.nn.functional.relu(lResult)
        
        lResult=self.mConv2(lResult)
        if self.__mShowOnce==False:
            print('Conv2: {:}'.format(lResult.shape))
        lResult=torch.nn.functional.relu(lResult)
        
        lResult=self.mPool1(lResult)
        if self.__mShowOnce==False:
            print('Pool1: {:}'.format(lResult.shape))
        
        lResult=self.mConv3(lResult)
        if self.__mShowOnce==False:
            print('Conv3: {:}'.format(lResult.shape))
        lResult=torch.nn.functional.relu(lResult)
        
        lResult=self.mConv4(lResult)
        if self.__mShowOnce==False:
            print('Conv4: {:}'.format(lResult.shape))
        lResult=torch.nn.functional.relu(lResult)
        
        lResult=self.mPool2(lResult)
        if self.__mShowOnce==False:
            print('Pool2: {:}'.format(lResult.shape))        
        
        lResult=lResult.flatten(start_dim=1)
        if self.__mShowOnce==False:
            print('Flat: {:}'.format(lResult.shape))

        lResult=self.mLin1(lResult)
        lResult=torch.nn.functional.relu(lResult)
        lResult=self.mLin2(lResult)
        lResult=torch.nn.functional.relu(lResult)
        lResult=self.mLin3(lResult)
        #lResult=torch.nn.functional.softmax(lResult)
        
        if self.__mShowOnce==False:
            self.__mShowOnce=True
            
        return lResult
#==================================================================================================


#**************************************************************************************************
#ModelBase
#--------------------------------------------------------------------------------------------------
class ModelBase:
    def __init__(self, aModelName, aModelPath, aDataSet):
        self.mModelName=aModelName
        self.mPath=aModelPath
        self.mDataSet=aDataSet
       
#==================================================================================================


#**************************************************************************************************
#Cvn model
#--------------------------------------------------------------------------------------------------
class CvnCls(ModelBase):
    def __init__(self, aPath, aDataSet, aEpochNum=20, aBatchSize=50):
        ModelBase.__init__(self, 'Convolutional neural network', aPath, aDataSet)
        self.mEpochNum=aEpochNum
        self.mBatchSize=aBatchSize
        
    def Train(self):
        print('{:} | Train start'.format(self.mModelName))
        torch.manual_seed(0)
        lTrainSclX, lTrainSclY=self.mDataSet.GetScaledData('train')
#       lTrainSclX, lTrainSclY=lTrainSclX.to(lDevice), lTrainSclY.to(lDevice)
    
        lCvNet=CvNet()
#       if lDevice=='cuda':
#           lCvNet=torch.nn.DataParallel(lCvNet, device_ids=range(torch.cuda.device_count()))
#           torch.backends.cudnn.benchmark=True

        lCriterion=torch.nn.CrossEntropyLoss()
        lOptimizer=torch.optim.SGD(lCvNet.parameters(), lr=0.01, momentum=0.9)    
        for lEpoch in range(self.mEpochNum):
            lEpochLoss=0.0
            lEpochTotal=0
            lEpochCorrect=0
            for lIndex in range(0, len(lTrainSclX), self.mBatchSize):        
                lBatchX=lTrainSclX[lIndex:lIndex+self.mBatchSize]
                lBatchY=lTrainSclY[lIndex:lIndex+self.mBatchSize]
                lOptimizer.zero_grad()
                lOutput=lCvNet(lBatchX)
                lLoss=lCriterion(lOutput, lBatchY)
                lLoss.backward()
                lOptimizer.step()
                lEpochLoss=lLoss.item()
                _, lPredictionIndex=lOutput.max(1)
                lEpochTotal+=lBatchY.size(0)
                lEpochCorrect+=lPredictionIndex.eq(lBatchY).sum().item()
            print('{:} | Epoch={:}, loss={:.3f}, Accuarcy={:.3f}%'.format(self.mModelName, lEpoch, lEpochLoss, 100*lEpochCorrect/lEpochTotal))
        lModelFile=os.path.join(self.mPath, self.mModelName+'.model')
        torch.save(lCvNet.state_dict(), lModelFile)
        lCorrect=0
        with torch.no_grad():
            lOutput=lCvNet(lTrainSclX)
            _, lPredictionIndex=lOutput.max(1)
            lCorrect=lPredictionIndex.eq(lTrainSclY).sum().item()
        print('{:} | Train complete'.format(self.mModelName))
        return (self.mModelName, lCorrect/lTrainSclY.size(0))
        
    def Test(self):
        print('{:} | Test start'.format(self.mModelName))
        lModelFile=os.path.join(self.mPath, self.mModelName+'.model')
        lCvNet=CvNet()
        lCvNet.load_state_dict(torch.load(lModelFile))
        lTestSclX, lTestSclY=self.mDataSet.GetScaledData('test')
        lCorrect=0
        with torch.no_grad():
            lOutput=lCvNet(lTestSclX)
            _, lPredictionIndex=lOutput.max(1)
            lCorrect=lPredictionIndex.eq(lTestSclY).sum().item()
        print('{:} | Test complete'.format(self.mModelName))
        return (self.mModelName, lCorrect/lTestSclY.size(0))
    
    def Interp(self):
        CAT_INDEX=8
        lModelFile=os.path.join(self.mPath, self.mModelName+'.model')
        lCvNet=CvNet()
        lCvNet.load_state_dict(torch.load(lModelFile))
        lTestImgX, lTestImgY=self.mDataSet.GetImgData('test')
        lTestSclX, lTestSclY=self.mDataSet.GetScaledData('test')
        lExpImg, lExpTorch=self.mDataSet.GetNonsense()
        
        lCatOutput=torch.nn.functional.softmax(lCvNet(torch.unsqueeze(lTestSclX[CAT_INDEX], 0)), dim=1)
        lInitialOutput=torch.nn.functional.softmax(lCvNet(lExpTorch), dim=1)

        for lIndex in range(3000):
            lOutput=lCvNet(lExpTorch)
            lOutput[0, 3].backward()
            lExpTorch.data+=0.0001*lExpTorch.grad.data
            lExpTorch.grad.data.zero_()
        
        lFinalOutput=torch.nn.functional.softmax(lCvNet(lExpTorch), dim=1)
        print('Cat input response: {:}'.format(lCatOutput))
        print('Initial input response {:}'.format(lInitialOutput))
        print('Final input response {:}'.format(lFinalOutput))
        
        lResImg=lExpTorch.detach().numpy()
        lResImg=lResImg.reshape(-1, 32, 32, 3)
        lResImg=np.interp(lResImg, (np.amin(lResImg), np.amax(lResImg)), (0, 255)).astype(np.uint8)
        
        return (lTestImgX[CAT_INDEX], lExpImg[0], lResImg[0])

#==================================================================================================        


#**************************************************************************************************
#Dt model
#--------------------------------------------------------------------------------------------------
class DtCls(ModelBase):
    def __init__(self, aPath, aDataSet, aMaxDepth=10000):
        ModelBase.__init__(self, 'Decision tree', aPath, aDataSet)
        self.mMaxDepth=aMaxDepth
        
    def Train(self):
        print('{:} | Train start'.format(self.mModelName))
        lTrainX, lTrainY=self.mDataSet.GetRawData('train')
        lDecisionTree=sklearn.tree.DecisionTreeClassifier(max_depth=self.mMaxDepth, criterion='entropy', random_state=0)
        lDecisionTree.fit(lTrainX, lTrainY)        
        if not os.path.exists(self.mPath):
            os.makedirs(self.mPath)
        lModelFile=os.path.join(self.mPath, self.mModelName+'.model')
        pickle.dump(lDecisionTree, open(lModelFile, 'wb'))
        print('{:} | Train complete'.format(self.mModelName))
        return (self.mModelName, lDecisionTree.score(lTrainX, lTrainY))
        
    def Test(self):
        print('{:} | Test start'.format(self.mModelName))
        lModelFile=os.path.join(self.mPath, self.mModelName+'.model')
        lDecisionTree=pickle.load(open(lModelFile, 'rb'))
        lTestX, lTestY=self.mDataSet.GetRawData('test')
        print('{:} | Test complete'.format(self.mModelName))
        plt.figure(figsize=(15,10))
        sklearn.tree.plot_tree(lDecisionTree, max_depth=4)
        return (self.mModelName, lDecisionTree.score(lTestX, lTestY))
        
#==================================================================================================        


class Action(enum.Enum):
    TRAIN=0
    TEST=1
    DATA=2
    INTERP=4

def Usage():
    print('Commandline-options:')
    print('-a, --action\t\tAction to perform can be either \'train\', \'test\', \'data\' or \'interp\'')
    print('-d, --datapath\t\tpath to directory with data set directories')
    print('-m, --modelpath\t\tpath to directory with trained models')
    print('-h, --help\t\tthis page')
    
def main():
    lAction=None
    lDataPath=None
    lModelPath=None
    lDevice=torch.device("cuda" if torch.cuda.is_available() else "cpu")
    try:
        lOpts, _= getopt.getopt(sys.argv[1:], 'ha:d:m:', ['help', 'action=', 'datapath=', 'modelpath='])
    except getopt.GetoptError as aErr:
        # print help information and exit:
        print(aErr)
        Usage()
        sys.exit(2)
    for lOption, lArgument in lOpts:
        if lOption in ('-a', '--action'):
            if lArgument=='train':
                lAction=Action.TRAIN
            elif lArgument=='test':
                lAction=Action.TEST
            elif lArgument=='data':
                lAction=Action.DATA
            elif lArgument=='interp':
                lAction=Action.INTERP
            else:
                Usage()
                sys.exit(2)
        elif lOption in ("-d", "--datapath"):
            lDataPath=lArgument
        elif lOption in ("-m", "--modelpath"):
            lModelPath=lArgument
        elif lOption in ("-h", "--help"):
            Usage()
            sys.exit()
        else:
            assert False, "Unknown option"
    
    global CIFAR_PATH
    for lDataType, lPath in CIFAR_PATH.items():
        CIFAR_PATH[lDataType]=[os.path.join(lDataPath, lCifarFile) for lCifarFile in lPath]
    lCifarData=CifarData(CIFAR_PATH)
    lCnv=CvnCls(lModelPath, lCifarData, aEpochNum=30, aBatchSize=50)
    lDt=DtCls(lModelPath, lCifarData, aMaxDepth=500000)
    lAllModels=[lDt, lCnv]
        
    if lAction==Action.TRAIN:
        for lCurrentModel in lAllModels:
            lModelName, lAccuracy=lCurrentModel.Train()
            print('{:} | Train accuracy={:.3f}%'.format(lModelName, lAccuracy*100))
    elif lAction==Action.TEST:
        for lCurrentModel in lAllModels:
            lModelName, lAccuracy=lCurrentModel.Test()
            print('{:} | Test accuracy={:.3f}%'.format(lModelName, lAccuracy*100))
    elif lAction==Action.DATA:
        lTrainRawX, lTrainRawY=lCifarData.GetRawData('train')
        print('Shape of aggragated train raw data, X={:}, y={:}'.format(lTrainRawX.shape, lTrainRawY.shape))
        lTestRawX, lTestRawY=lCifarData.GetRawData('test')
        print('Shape of aggragated test raw data, X={:}, y={:}'.format(lTestRawX.shape, lTestRawY.shape))
        
        lTrainImgX, lTrainImgY=lCifarData.GetImgData('train')
        print('Shape of aggragated train image data, X={:}, y={:}'.format(lTrainImgX.shape, lTrainImgY.shape))
        print('First image in the data-set (X[0]):')
        plt.imshow(lTrainImgX[0])
    elif lAction==Action.INTERP:        
        lTestImage, lExpInit, lExpRes=lCnv.Interp()
        plt.imshow(lTestImage)
        plt.figure()
        plt.imshow(lExpInit)
        plt.figure()
        plt.imshow(lExpRes)
        
    else:
        print('ERROR')
        sys.exit(2)  

    
if __name__ == "__main__":
    main()
