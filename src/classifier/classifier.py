#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
import getopt
import enum
import os
import numpy as np
import sklearn
import sklearn.impute
import sklearn.metrics
import sklearn.multiclass
import sklearn.multioutput
import sklearn.neighbors
import sklearn.svm
import sklearn.tree
import sklearn.ensemble
import sklearn.naive_bayes
import sklearn.neural_network
import sklearn.preprocessing
import sklearn.datasets
import pandas as pd
import scipy
import pickle
import warnings
warnings.filterwarnings("ignore", category=FutureWarning)

ADULT_PATH={'train': 'Adult/adult.data'}
YEAST_PATH={'train': 'Yeast/yeast.data'}
CREDITDEFAULT_PATH={'train': 'Default of credit card clients/default of credit card clients.xls'}
THORACICSURGERY_PATH={'train': 'Thoracic Surgery/ThoraricSurgery.arff'}
SEISMICBUMPS_PATH={'train': 'Seismic-bumps/seismic-bumps.arff'}
DIABETICRTY_PATH={'train': 'Diabetic Retinopathy/messidor_features.arff'}
STEELPLTFLT_PATH={'train': 'Steel Plates Faults/Faults.NNA'}
GERMANCREDIT_PATH={'train': 'Statlog (German Credit Data)/german.data-numeric'}
AUSTRALIANCREDIT_PATH={'train': 'Statlog (Australian Credit Approval)/australian.dat'}
BRCANWISC_PATH={'train': 'Breast Cancer Wisconsin/breast-cancer-wisconsin.data'}
BRCANWISCDIAG_PATH={'train': 'Breast Cancer Wisconsin/wdbc.data'}
BRCANWISCPROG_PATH={'train': 'Breast Cancer Wisconsin/wpbc.data'}


#**************************************************************************************************
#Data processor
#--------------------------------------------------------------------------------------------------
class DataProcessor:
    def __init__(self, aDataSetName, aDataPath, aScoreMetric='accuracy', aSearchIterations=20):
        self.mDataSetName=aDataSetName
        self.mPath=aDataPath
        self.mScoreMetric=aScoreMetric
        self.mSearchIterations=aSearchIterations
        
    def GetDataSetName(self):
        return self.mDataSetName
    
    def GetScoreMetric(self):
        return self.mScoreMetric
    
    def GetSearchIterations(self):
        return self.mSearchIterations
        
    def ProcessData(self, aData, aHandling):
        lData=aData
        if aHandling=='filter':
            lData=lData[~np.isnan(lData).any(axis=1)]
        elif aHandling=='impute':
            lImp=sklearn.impute.SimpleImputer(missing_values=np.nan, strategy='most_frequent', copy=False)
            lImp.fit(lData)
            lData=lImp.transform(lData)
        return lData
#==================================================================================================


#**************************************************************************************************
#Adult data
#--------------------------------------------------------------------------------------------------
class AdultData(DataProcessor):
    def __init__(self, aPath, aSuffix=''):
        DataProcessor.__init__(self, 'Adult data'+aSuffix, aPath)
        
    def __AdultIntParser(self, aValue):
        lValue=aValue.strip()
        if '?'==lValue:
            return None
        else:
            return int(lValue)
        
    def __AdultWorkclassParser(self, aValue):
        lValueMapper={'Private': 0, 'Self-emp-not-inc': 1, 'Self-emp-inc': 2, 'Federal-gov': 3, 'Local-gov': 4, 'State-gov': 5, 'Without-pay': 6, 'Never-worked': 7}
        return lValueMapper.get(aValue.strip())
        
    def __AdultEducationParser(self, aValue):
        lValueMapper={'Bachelors': 0, 'Some-college': 1, '11th': 2, 'HS-grad': 3, 'Prof-school': 4, 'Assoc-acdm': 5, 'Assoc-voc': 6, '9th': 7, '7th-8th': 8, '12th': 9, 'Masters': 10, '1st-4th': 11, '10th': 12, 'Doctorate': 13, '5th-6th': 14, 'Preschool': 15}
        return lValueMapper.get(aValue.strip())
        
    def __AdultMtlstatParser(self, aValue):
        lValueMapper={'Married-civ-spouse': 0, 'Divorced': 1, 'Never-married': 2, 'Separated': 3, 'Widowed': 4, 'Married-spouse-absent': 5, 'Married-AF-spouse': 6}
        return lValueMapper.get(aValue.strip())
    
    def __AdultOccParser(self, aValue):
        lValueMapper={'Tech-support': 0, 'Craft-repair': 1, 'Other-service': 2, 'Sales': 3, 'Exec-managerial': 4, 'Prof-specialty': 5, 'Handlers-cleaners': 6, 'Machine-op-inspct': 7, 'Adm-clerical': 8, 'Farming-fishing': 9, 'Transport-moving': 10, 'Priv-house-serv': 11, 'Protective-serv': 12, 'Armed-Forces': 13}
        return lValueMapper.get(aValue.strip())
    
    def __AdultRlspParser(self, aValue):
        lValueMapper={'Wife': 0, 'Own-child': 1, 'Husband': 2, 'Not-in-family': 3, 'Other-relative': 4, 'Unmarried': 5}
        return lValueMapper.get(aValue.strip())
    
    def __AdultRaceParser(self, aValue):
        lValueMapper={'White': 0, 'Asian-Pac-Islander': 1, 'Amer-Indian-Eskimo': 2, 'Other': 3, 'Black': 4}
        return lValueMapper.get(aValue.strip())
    
    def __AdultSexParser(self, aValue):
        lValueMapper={'Female': 0, 'Male': 1}
        return lValueMapper.get(aValue.strip())
    
    def __AdultNtvctrParser(self, aValue):
        lValueMapper={'United-States': 0, 'Cambodia': 1, 'England': 2, 'Puerto-Rico': 3, 'Canada': 4, 'Germany': 5, 'Outlying-US(Guam-USVI-etc)': 6, 'India': 7, 'Japan': 8, 'Greece': 9, 'South': 10, 'China': 11, 'Cuba': 12, 'Iran': 13, 'Honduras': 14, 'Philippines': 15, 'Italy': 16, 'Poland': 17, 'Jamaica': 18, 'Vietnam': 19, 'Mexico': 20, 'Portugal': 21, 'Ireland': 22, 'France': 23, 'Dominican-Republic': 24, 'Laos': 25, 'Ecuador': 26, 'Taiwan': 27, 'Haiti': 28, 'Columbia': 29, 'Hungary': 30, 'Guatemala': 31, 'Nicaragua': 32, 'Scotland': 33, 'Thailand': 34, 'Yugoslavia': 35, 'El-Salvador': 36, 'Trinadad&Tobago': 37, 'Peru': 38, 'Hong': 39, 'Holand-Netherlands': 40}
        return lValueMapper.get(aValue.strip())
    
    def __AdultSalaryParser(self, aValue):
        if '>50K'==aValue.strip():
            return 1
        else:
            return 0
    
    def GetData(self, aDataType, aHandling='filter'):
        lPath=self.mPath[aDataType]
        lAdultConv={0: self.__AdultIntParser, 1: self.__AdultWorkclassParser, 2: self.__AdultIntParser, 3: self.__AdultEducationParser, 4: self.__AdultIntParser, 5: self.__AdultMtlstatParser, 6: self.__AdultOccParser, 7: self.__AdultRlspParser, 8: self.__AdultRaceParser, 9: self.__AdultSexParser, 10: self.__AdultIntParser, 11: self.__AdultIntParser, 12: self.__AdultIntParser, 13: self.__AdultNtvctrParser, 14: self.__AdultSalaryParser}
        lAdultData=np.loadtxt(lPath, delimiter=',', converters=lAdultConv, encoding='UTF-8', dtype=np.float64)
        lAdultData=super().ProcessData(lAdultData, aHandling)
        return (lAdultData[:,:-1], lAdultData[:,-1].astype(int))
#==================================================================================================    


#**************************************************************************************************
#Yeast data
#--------------------------------------------------------------------------------------------------
class YeastData(DataProcessor):
    def __init__(self, aPath, aSuffix=''):
        DataProcessor.__init__(self, 'Yeast data'+aSuffix, aPath)
        
    def __YeastFloatParser(self, aValue):
        try:
            return float(aValue.strip())
        except:
            return None
        
    def __YeastLocParser(self, aValue):
        lValueMapper={'CYT': 0, 'NUC': 1, 'MIT': 2, 'ME3': 3, 'ME2': 4, 'ME1': 5, 'EXC': 6, 'VAC': 7, 'POX': 8, 'ERL': 9} 
        return lValueMapper.get(aValue)
        
    def GetData(self, aDataType, aHandling='filter'):
        lPath=self.mPath[aDataType]
        lYeastConv={0: self.__YeastFloatParser, 1: self.__YeastFloatParser, 2: self.__YeastFloatParser, 3: self.__YeastFloatParser, 4: self.__YeastFloatParser, 5: self.__YeastFloatParser, 6: self.__YeastFloatParser, 7: self.__YeastFloatParser, 8: self.__YeastFloatParser, 9: self.__YeastLocParser}
        lYeastData=np.loadtxt(lPath, delimiter='  ', converters=lYeastConv, usecols=range(1, 10), encoding='UTF-8', dtype=np.float64)
        lYeastData=super().ProcessData(lYeastData, aHandling)
        return (lYeastData[:,:-1], lYeastData[:,-1].astype(int))
#==================================================================================================        


#**************************************************************************************************
#CreditDefault data
#--------------------------------------------------------------------------------------------------
class CreditDefaultData(DataProcessor):
    def __init__(self, aPath, aSuffix=''):
        DataProcessor.__init__(self, 'Credit default data'+aSuffix, aPath)
        
    def GetData(self, aDataType, aHandling='filter'):
        lPath=self.mPath[aDataType]
        lCreditDefaultData=np.array(pd.read_excel(lPath, usecols='B:Y', header=None, skiprows=range(2)))
        lCreditDefaultData=super().ProcessData(lCreditDefaultData, aHandling)
        return (lCreditDefaultData[:,:-1], lCreditDefaultData[:,-1].astype(int))
#==================================================================================================        


#**************************************************************************************************
#ThoracicSurgery data
#--------------------------------------------------------------------------------------------------
class ThoracicSurgeryData(DataProcessor):
    def __init__(self, aPath, aSuffix=''):
        DataProcessor.__init__(self, 'Thoracic surgery data'+aSuffix, aPath)
        
    def __ThoracicSurgeryDiagCodeParser(self, aValue):
        lValueMapper={'DGN3': 0, 'DGN2': 1, 'DGN4': 2, 'DGN6': 3, 'DGN5': 4, 'DGN8': 5, 'DGN1': 6}
        return lValueMapper.get(aValue)
    
    def __ThoracicSurgeryPerfStatusParser(self, aValue):
        lValueMapper={'PRZ2': 0, 'PRZ1': 1, 'PRZ0': 2}
        return lValueMapper.get(aValue)
    
    def __ThoracicSurgeryClinicalTnmParser(self, aValue):
        lValueMapper={'OC11': 0, 'OC14': 1, 'OC12': 2, 'OC13': 3}
        return lValueMapper.get(aValue)
    
    def __ThoracicSurgeryBooleanParser(self, aValue):
        if aValue=='T':
            return 1
        else:
            return 0
        
    def __ThoracicSurgeryFloatParser(self, aValue):
        try:
            return float(aValue)
        except:
            return None
        
    def __ThoracicSurgeryIntParser(self, aValue):
        try:
            return int(aValue)
        except:
            return None
        
    def GetData(self, aDataType, aHandling='filter'):
        lPath=self.mPath[aDataType]
        lThoracicSurgeryConv={0: self.__ThoracicSurgeryDiagCodeParser, 1: self.__ThoracicSurgeryFloatParser, 2: self.__ThoracicSurgeryFloatParser, 3: self.__ThoracicSurgeryPerfStatusParser, 4: self.__ThoracicSurgeryBooleanParser, 5: self.__ThoracicSurgeryBooleanParser, 6: self.__ThoracicSurgeryBooleanParser, 7: self.__ThoracicSurgeryBooleanParser, 8: self.__ThoracicSurgeryBooleanParser, 9: self.__ThoracicSurgeryClinicalTnmParser, 10: self.__ThoracicSurgeryBooleanParser, 11: self.__ThoracicSurgeryBooleanParser, 12: self.__ThoracicSurgeryBooleanParser, 13: self.__ThoracicSurgeryBooleanParser, 14: self.__ThoracicSurgeryBooleanParser, 15: self.__ThoracicSurgeryIntParser, 16: self.__ThoracicSurgeryBooleanParser}
        lThoracicSurgeryData=np.loadtxt(lPath, delimiter=',', skiprows=21, converters=lThoracicSurgeryConv, encoding='UTF-8', dtype=np.float64)
        lThoracicSurgeryData=super().ProcessData(lThoracicSurgeryData, aHandling)
        return (lThoracicSurgeryData[:,:-1], lThoracicSurgeryData[:,-1].astype(int))
#==================================================================================================        


#**************************************************************************************************
#SeismicBumps data
#--------------------------------------------------------------------------------------------------
class SeismicBumpsData(DataProcessor):
    def __init__(self, aPath, aSuffix=''):
        DataProcessor.__init__(self, 'Seismic bumps data'+aSuffix, aPath)
        
    def __SeismicSeismicParser(self, aValue):
        lValueMapper={'a': 0, 'b': 1, 'c': 2, 'd': 3}
        return lValueMapper.get(aValue)
    
    def __SeismicSeismicAccParser(self, aValue):
        lValueMapper={'a': 0, 'b': 1, 'c': 2, 'd': 3}
        return lValueMapper.get(aValue)
    
    def __SeismicShiftParser(self, aValue):
        lValueMapper={'W': 0, 'N': 1}
        return lValueMapper.get(aValue)
    
    def __SeismicHazardParser(self, aValue):
        lValueMapper={'a': 0, 'b': 1, 'c': 2, 'd': 3}
        return lValueMapper.get(aValue)
            
    def __SeismicFloatParser(self, aValue):
        try:
            return float(aValue)
        except:
            return None
        
    def __SeismicIntParser(self, aValue):
        try:
            return int(aValue)
        except:
            return None
        
    def GetData(self, aDataType, aHandling='filter'):
        lPath=self.mPath[aDataType]
        lSeismicConv={0: self.__SeismicSeismicParser, 1: self.__SeismicSeismicAccParser, 2: self.__SeismicShiftParser, 3: self.__SeismicIntParser, 4: self.__SeismicIntParser, 5: self.__SeismicIntParser, 6: self.__SeismicIntParser, 7: self.__SeismicHazardParser, 8: self.__SeismicIntParser, 9: self.__SeismicIntParser, 10: self.__SeismicIntParser, 11: self.__SeismicIntParser, 12: self.__SeismicIntParser, 13: self.__SeismicIntParser, 14: self.__SeismicIntParser, 15: self.__SeismicIntParser, 16: self.__SeismicIntParser, 17: self.__SeismicIntParser, 18: self.__SeismicIntParser}
        lSeismicData=np.loadtxt(lPath, delimiter=',', skiprows=154, converters=lSeismicConv, encoding='UTF-8', dtype=np.float64)
        lSeismicData=super().ProcessData(lSeismicData, aHandling)
        return (lSeismicData[:,:-1], lSeismicData[:,-1].astype(int))
#==================================================================================================
   
     
#**************************************************************************************************
#DiabeticRty data
#--------------------------------------------------------------------------------------------------
class DiabeticRtyData(DataProcessor):
    def __init__(self, aPath, aSuffix=''):
        DataProcessor.__init__(self, 'Diabetic retinopathy data'+aSuffix, aPath)
    
    def GetData(self, aDataType, aHandling='filter'):
        lPath=self.mPath[aDataType]
        lDiabeticRtyData=np.loadtxt(lPath, delimiter=',', skiprows=24, encoding='UTF-8', dtype=np.float64)
        lDiabeticRtyData=super().ProcessData(lDiabeticRtyData, aHandling)
        return (lDiabeticRtyData[:,:-1], lDiabeticRtyData[:,-1].astype(int))
#================================================================================================== 
        

#**************************************************************************************************
#SteelPltFlt data
#--------------------------------------------------------------------------------------------------
class SteelPltFlt(DataProcessor):
    def __init__(self, aPath, aSuffix=''):
        DataProcessor.__init__(self, 'Steel plates faults data'+aSuffix, aPath)
    
    def GetData(self, aDataType, aHandling='filter'):
        lPath=self.mPath[aDataType]
        lSteelPltFltData=np.loadtxt(lPath, delimiter='\t', encoding='UTF-8', dtype=np.float64)
        lSteelPltFltData=super().ProcessData(lSteelPltFltData, aHandling)
        #return (lSteelPltFltData[:,:-7], lSteelPltFltData[:,-7:].astype(int))
        return (lSteelPltFltData[:,:-7], np.argwhere(lSteelPltFltData[:,-7:].astype(int))[:,1])
#================================================================================================== 


#**************************************************************************************************
#GermanCredit data
#--------------------------------------------------------------------------------------------------
class GermanCredit(DataProcessor):
    def __init__(self, aPath, aSuffix=''):
        DataProcessor.__init__(self, 'German credit data'+aSuffix, aPath)
    
    def GetData(self, aDataType, aHandling='filter'):
        lPath=self.mPath[aDataType]
        lGermanCreditData=np.loadtxt(lPath, encoding='UTF-8', dtype=np.int32)
        lGermanCreditData=super().ProcessData(lGermanCreditData, aHandling)
        return (lGermanCreditData[:,:-1], lGermanCreditData[:,-1].astype(int)-1)
#================================================================================================== 


#**************************************************************************************************
#AustralianCredit data
#--------------------------------------------------------------------------------------------------
class AustralianCredit(DataProcessor):
    def __init__(self, aPath, aSuffix=''):
        DataProcessor.__init__(self, 'Australian credit approval data'+aSuffix, aPath)
    
    def GetData(self, aDataType, aHandling='filter'):
        lPath=self.mPath[aDataType]
        lAustralianCreditData=np.loadtxt(lPath, encoding='UTF-8', dtype=np.float64)
        lAustralianCreditData=super().ProcessData(lAustralianCreditData, aHandling)
        return (lAustralianCreditData[:,:-1], lAustralianCreditData[:,-1].astype(int))
#================================================================================================== 


#**************************************************************************************************
#BrCanWisc data
#--------------------------------------------------------------------------------------------------
class BrCanWisc(DataProcessor):
    def __init__(self, aPath, aSuffix=''):
        DataProcessor.__init__(self, 'Breast cancer Wisconsin data'+aSuffix, aPath)
        
    def __BrCanWiscIntParser(self, aValue):
        try:
            return int(aValue)
        except:
            return None
    
    def GetData(self, aDataType, aHandling='filter'):
        lPath=self.mPath[aDataType]
        lBrCanWiscConv={lCounter: self.__BrCanWiscIntParser for(lCounter) in range(11)}
        lBrCanWiscData=np.loadtxt(lPath, delimiter=',', converters=lBrCanWiscConv, encoding='UTF-8', dtype=np.float64)
        lBrCanWiscData=super().ProcessData(lBrCanWiscData, aHandling)
        return (lBrCanWiscData[:,1:-1].astype(int), ((lBrCanWiscData[:,-1].astype(int)-2)/2).astype(int))
#================================================================================================== 


#**************************************************************************************************
#BrCanWiscProg data
#--------------------------------------------------------------------------------------------------
class BrCanWiscProg(DataProcessor):
    def __init__(self, aPath, aSuffix=''):
        DataProcessor.__init__(self, 'Breast cancer Wisconsin prognosis data'+aSuffix, aPath)
    
    def __BrCanWiscProgIntParser(self, aValue):
        try:
            return int(aValue)
        except:
            return None    
    
    def __BrCanWiscProgFloatParser(self, aValue):
        try:
            return float(aValue)
        except:
            return None
        
    def __BrCanWiscProgOutcomeParser(self, aValue):
        if 'N'==aValue:
            return 1
        elif 'R'==aValue:
            return 0
        else:
            return None
    
    def GetData(self, aDataType, aHandling='filter'):
        lPath=self.mPath[aDataType]
        lBrCanWiscProgConv={0: self.__BrCanWiscProgIntParser, 1: self.__BrCanWiscProgOutcomeParser, 2: self.__BrCanWiscProgIntParser}
        lBrCanWiscProgConv.update({lCounter: self.__BrCanWiscProgFloatParser for(lCounter) in range(3, 35)})
        lBrCanWiscProgData=np.loadtxt(lPath, delimiter=',', converters=lBrCanWiscProgConv, encoding='UTF-8', dtype=np.float64)
        lBrCanWiscProgData=super().ProcessData(lBrCanWiscProgData, aHandling)
        return (lBrCanWiscProgData[:,3:], lBrCanWiscProgData[:, 1].astype(int))
#================================================================================================== 
   
     
#**************************************************************************************************
#BrCanWiscDiag data
#--------------------------------------------------------------------------------------------------
class BrCanWiscDiag(DataProcessor):
    def __init__(self, aPath, aSuffix=''):
        DataProcessor.__init__(self, 'Breast cancer Wisconsin diagnostic data'+aSuffix, aPath)
    
    def __BrCanWiscDiagIntParser(self, aValue):
        try:
            return int(aValue)
        except:
            return None    
    
    def __BrCanWiscDiagFloatParser(self, aValue):
        try:
            return float(aValue)
        except:
            return None
        
    def __BrCanWiscDiagDiagParser(self, aValue):
        if 'M'==aValue:
            return 1
        elif 'B'==aValue:
            return 0
        else:
            return None
    
    def GetData(self, aDataType, aHandling='filter'):
        lPath=self.mPath[aDataType]
        lBrCanWiscDiagConv={0: self.__BrCanWiscDiagIntParser, 1: self.__BrCanWiscDiagDiagParser}
        lBrCanWiscDiagConv.update({lCounter: self.__BrCanWiscDiagFloatParser for(lCounter) in range(2, 32)})
        lBrCanWiscDiagData=np.loadtxt(lPath, delimiter=',', converters=lBrCanWiscDiagConv, encoding='UTF-8', dtype=np.float64)
        lBrCanWiscDiagData=super().ProcessData(lBrCanWiscDiagData, aHandling)
        return (lBrCanWiscDiagData[:,2:], lBrCanWiscDiagData[:, 1].astype(int))
#==================================================================================================     



#**************************************************************************************************
#ModelBase
#--------------------------------------------------------------------------------------------------
class ModelBase:
    def __init__(self, aModelName, aModelPath, aDataSet):
        self.mModelName=aModelName
        self.mPath=aModelPath
        self.mDataSet=aDataSet        
        self.mIterationNum=aDataSet.GetSearchIterations()
        self.mScore=aDataSet.GetScoreMetric()
        self.mCvGen=sklearn.model_selection.KFold(n_splits=10, random_state=0)
        self.mPrintInfo=True
        
    def GetModelName(self):
        return self.mModelName
    
    def SaveModel(self, aModel):
        if not os.path.exists(self.mPath):
            os.makedirs(self.mPath)
        lModelFile=os.path.join(self.mPath, self.mModelName+'_'+self.mDataSet.GetDataSetName()+'.model')
        pickle.dump(aModel, open(lModelFile, 'wb'), protocol=4)
        
    def LoadModel(self):
        lModelFile=os.path.join(self.mPath, self.mModelName+'_'+self.mDataSet.GetDataSetName()+'.model')
        return pickle.load(open(lModelFile, 'rb'))
    
    def TestEvaluateCv(self):
        lX, lY=self.mDataSet.GetData('train')
        lClsModel=self.LoadModel()
        lScore=sklearn.model_selection.cross_val_score(lClsModel, lX, lY, scoring=self.mScore, cv=self.mCvGen, n_jobs=-1, verbose=0, error_score='raise')
        return (self.mModelName, self.mDataSet.GetDataSetName(), np.mean(lScore))
    
#==================================================================================================


#**************************************************************************************************
#KngCls model
#--------------------------------------------------------------------------------------------------
class KngCls(ModelBase):
    def __init__(self, aPath, aDataSet, aMinNb=3, aMaxNb=100):
        ModelBase.__init__(self, 'K-nearest neighbours', aPath, aDataSet)
        self.mMinNb=aMinNb
        self.mMaxNb=aMaxNb
        
    def TrainEvaluateCv(self):
        if self.mPrintInfo==True:
            print('{:}|{:}| Start, iterations={:}, score={:}'.format(self.mModelName, self.mDataSet.GetDataSetName(), self.mIterationNum, self.mScore))
        lXTrain, lYTrain=self.mDataSet.GetData('train')
        lClsModel=sklearn.neighbors.KNeighborsClassifier(n_jobs=-1)
        lClsModelPrm={'n_neighbors': scipy.stats.randint(low=self.mMinNb, high=self.mMaxNb)}
        lClsModelRndCv=sklearn.model_selection.RandomizedSearchCV(lClsModel, lClsModelPrm, n_iter=self.mIterationNum, scoring=self.mScore, n_jobs=-1, iid=False, cv=self.mCvGen, verbose=0, random_state=0, error_score='raise')
        lClsModelRndCv.fit(lXTrain, lYTrain)
        if self.mPrintInfo==True:
            print('{:}|{:}| Finish, best params={:}'.format(self.mModelName, self.mDataSet.GetDataSetName(), lClsModelRndCv.best_params_))
        super().SaveModel(lClsModelRndCv.best_estimator_)
        return (self.mModelName, self.mDataSet.GetDataSetName(), lClsModelRndCv.best_score_)

#==================================================================================================


#**************************************************************************************************
#SVM model
#--------------------------------------------------------------------------------------------------
class SvmCls(ModelBase):
    def __init__(self, aPath, aDataSet, aMinC=1.0, aMaxC=1000.0, aMinGamma=0.001, aMaxGamma=10):
        ModelBase.__init__(self, 'SVM classification', aPath, aDataSet)
        self.mMinC=aMinC
        self.mMaxC=aMaxC
        self.mMinGamma=aMinGamma
        self.mMaxGamma=aMaxGamma
        
    def TrainEvaluateCv(self):
        if self.mPrintInfo==True:
            print('{:}|{:}| Start, iterations={:}, score={:}'.format(self.mModelName, self.mDataSet.GetDataSetName(), self.mIterationNum, self.mScore))
        lXTrain, lYTrain=self.mDataSet.GetData('train')        

#RA: Code below should have worked with multi-label classification
# =============================================================================
#         lClsModel=None
#         lClsModelPrm=None
#         if lYTrain.ndim>1:
#             lClsModel=sklearn.multiclass.OneVsRestClassifier(sklearn.svm.SVC(kernel='rbf', random_state=0), n_jobs=-1)
#             lClsModelPrm={'estimator__C': scipy.stats.reciprocal(self.mMinC, self.mMaxC), 'estimator__gamma': scipy.stats.reciprocal(self.mMinGamma, self.mMaxGamma)}
#         else:
#             lClsModel=sklearn.svm.SVC(kernel='rbf', random_state=0)
#             lClsModelPrm={'C': scipy.stats.reciprocal(self.mMinC, self.mMaxC), 'gamma': scipy.stats.reciprocal(self.mMinGamma, self.mMaxGamma)}
# =============================================================================
        lClsModel=sklearn.svm.SVC(kernel='rbf', random_state=0)
        lClsModelPrm={'C': scipy.stats.reciprocal(self.mMinC, self.mMaxC), 'gamma': scipy.stats.reciprocal(self.mMinGamma, self.mMaxGamma)}
        lClsModelRndCv=sklearn.model_selection.RandomizedSearchCV(lClsModel, lClsModelPrm, n_iter=self.mIterationNum, scoring=self.mScore, n_jobs=-1, iid=False, cv=self.mCvGen, verbose=0, random_state=0, error_score='raise')
        lClsModelRndCv.fit(lXTrain, lYTrain)
        if self.mPrintInfo==True:
            print('{:}|{:}| Finish, best params={:}'.format(self.mModelName, self.mDataSet.GetDataSetName(), lClsModelRndCv.best_params_))
        super().SaveModel(lClsModelRndCv.best_estimator_)
        return (self.mModelName, self.mDataSet.GetDataSetName(), lClsModelRndCv.best_score_)

#==================================================================================================


#**************************************************************************************************
#DT model
#--------------------------------------------------------------------------------------------------
class DtCls(ModelBase):
    def __init__(self, aPath, aDataSet, aMinMaxDepth=10, aMaxMaxDepth=300):
        ModelBase.__init__(self, 'Decision Tree Classifier', aPath, aDataSet)        
        self.mMinMaxDepth=aMinMaxDepth
        self.mMaxMaxDepth=aMaxMaxDepth
        
    def TrainEvaluateCv(self):
        if self.mPrintInfo==True:
            print('{:}|{:}| Start, iterations={:}, score={:}'.format(self.mModelName, self.mDataSet.GetDataSetName(), self.mIterationNum, self.mScore))
        lXTrain, lYTrain=self.mDataSet.GetData('train')
        lClsModel=sklearn.tree.DecisionTreeClassifier(random_state=0)
        lClsModelPrm={'max_depth': scipy.stats.randint(low=self.mMinMaxDepth, high=self.mMaxMaxDepth)}
        lClsModelRndCv=sklearn.model_selection.RandomizedSearchCV(lClsModel, lClsModelPrm, n_iter=self.mIterationNum, scoring=self.mScore, n_jobs=-1, iid=False, cv=self.mCvGen, verbose=0, random_state=0, error_score='raise')
        lClsModelRndCv.fit(lXTrain, lYTrain)
        if self.mPrintInfo==True:
            print('{:}|{:}| Finish, best params={:}'.format(self.mModelName, self.mDataSet.GetDataSetName(), lClsModelRndCv.best_params_))
        super().SaveModel(lClsModelRndCv.best_estimator_)
        return (self.mModelName, self.mDataSet.GetDataSetName(), lClsModelRndCv.best_score_)
       
#==================================================================================================


#**************************************************************************************************
#RF model
#--------------------------------------------------------------------------------------------------
class RfCls(ModelBase):
    def __init__(self, aPath, aDataSet, aMinEstimators=10, aMaxEstimators=300, aMinMaxDepth=10, aMaxMaxDepth=300):
        ModelBase.__init__(self, 'Random Forest Classifier', aPath, aDataSet)        
        self.mMinEstimators=aMinEstimators
        self.mMaxEstimators=aMaxEstimators
        self.mMinMaxDepth=aMinMaxDepth
        self.mMaxMaxDepth=aMaxMaxDepth
        
    def TrainEvaluateCv(self):
        if self.mPrintInfo==True:
            print('{:}|{:}| Start, iterations={:}, score={:}'.format(self.mModelName, self.mDataSet.GetDataSetName(), self.mIterationNum, self.mScore))
        lXTrain, lYTrain=self.mDataSet.GetData('train')
        lClsModel=sklearn.ensemble.RandomForestClassifier(random_state=0)
        lClsModelPrm={'n_estimators': scipy.stats.randint(low=self.mMinEstimators, high=self.mMaxEstimators), 'max_depth': scipy.stats.randint(low=self.mMinMaxDepth, high=self.mMaxMaxDepth)}
        lClsModelRndCv=sklearn.model_selection.RandomizedSearchCV(lClsModel, lClsModelPrm, n_iter=self.mIterationNum, scoring=self.mScore, n_jobs=-1, iid=False, cv=self.mCvGen, verbose=0, random_state=0, error_score='raise')
        lClsModelRndCv.fit(lXTrain, lYTrain)
        if self.mPrintInfo==True:
            print('{:}|{:}| Finish, best params={:}'.format(self.mModelName, self.mDataSet.GetDataSetName(), lClsModelRndCv.best_params_))
        super().SaveModel(lClsModelRndCv.best_estimator_)
        return (self.mModelName, self.mDataSet.GetDataSetName(), lClsModelRndCv.best_score_)
       
#==================================================================================================


#**************************************************************************************************
#AB model
#--------------------------------------------------------------------------------------------------
class AbCls(ModelBase):
    def __init__(self, aPath, aDataSet, aMinEstimators=10, aMaxEstimators=300):
        ModelBase.__init__(self, 'Ada Boost Classifier', aPath, aDataSet)        
        self.mMinEstimators=aMinEstimators
        self.mMaxEstimators=aMaxEstimators
        
    def TrainEvaluateCv(self):
        if self.mPrintInfo==True:
            print('{:}|{:}| Start, iterations={:}, score={:}'.format(self.mModelName, self.mDataSet.GetDataSetName(), self.mIterationNum, self.mScore))
        lXTrain, lYTrain=self.mDataSet.GetData('train')
        lClsModel=sklearn.ensemble.AdaBoostClassifier(random_state=0)
        lClsModelPrm={'n_estimators': scipy.stats.randint(low=self.mMinEstimators, high=self.mMaxEstimators)}
        lClsModelRndCv=sklearn.model_selection.RandomizedSearchCV(lClsModel, lClsModelPrm, n_iter=self.mIterationNum, scoring=self.mScore, n_jobs=-1, iid=False, cv=self.mCvGen, verbose=0, random_state=0, error_score='raise')
        lClsModelRndCv.fit(lXTrain, lYTrain)
        if self.mPrintInfo==True:
            print('{:}|{:}| Finish, best params={:}'.format(self.mModelName, self.mDataSet.GetDataSetName(), lClsModelRndCv.best_params_))
        super().SaveModel(lClsModelRndCv.best_estimator_)
        return (self.mModelName, self.mDataSet.GetDataSetName(), lClsModelRndCv.best_score_)
       
#==================================================================================================


#**************************************************************************************************
#LR model
#--------------------------------------------------------------------------------------------------
class LrCls(ModelBase):
    def __init__(self, aPath, aDataSet, aMinC=1.0, aMaxC=1000.0, aMinMaxIter=50, aMaxMaxIter=500):
        ModelBase.__init__(self, 'Logistic Regression Classifier', aPath, aDataSet)        
        self.mMinC=aMinC
        self.mMaxC=aMaxC
        self.mMinMaxIter=aMinMaxIter
        self.mMaxMaxIter=aMaxMaxIter
        
    def TrainEvaluateCv(self):
        if self.mPrintInfo==True:
            print('{:}|{:}| Start, iterations={:}, score={:}'.format(self.mModelName, self.mDataSet.GetDataSetName(), self.mIterationNum, self.mScore))
        lXTrain, lYTrain=self.mDataSet.GetData('train')
        lClsModel=sklearn.linear_model.LogisticRegression(random_state=0, multi_class='auto', n_jobs=-1)
        lClsModelPrm={'C': scipy.stats.reciprocal(self.mMinC, self.mMaxC), 'max_iter': scipy.stats.randint(low=self.mMinMaxIter, high=self.mMaxMaxIter)}
        lClsModelRndCv=sklearn.model_selection.RandomizedSearchCV(lClsModel, lClsModelPrm, n_iter=self.mIterationNum, scoring=self.mScore, n_jobs=-1, iid=False, cv=self.mCvGen, verbose=0, random_state=0, error_score='raise')
        lClsModelRndCv.fit(lXTrain, lYTrain)
        if self.mPrintInfo==True:
            print('{:}|{:}| Finish, best params={:}'.format(self.mModelName, self.mDataSet.GetDataSetName(), lClsModelRndCv.best_params_))
        super().SaveModel(lClsModelRndCv.best_estimator_)
        return (self.mModelName, self.mDataSet.GetDataSetName(), lClsModelRndCv.best_score_)
       
#==================================================================================================


#**************************************************************************************************
#Gnb model
#--------------------------------------------------------------------------------------------------
class GnbCls(ModelBase):
    def __init__(self, aPath, aDataSet):
        ModelBase.__init__(self, 'Gaussian Naive Bayes', aPath, aDataSet)        
        
    def TrainEvaluateCv(self):
        if self.mPrintInfo==True:
            print('{:}|{:}| Start, iterations={:}, score={:}'.format(self.mModelName, self.mDataSet.GetDataSetName(), self.mIterationNum, self.mScore))
        lXTrain, lYTrain=self.mDataSet.GetData('train')
        lClsModel=sklearn.naive_bayes.GaussianNB()
        lScore=sklearn.model_selection.cross_val_score(lClsModel, lXTrain, lYTrain, scoring=self.mScore, cv=self.mCvGen, n_jobs=-1, verbose=0, error_score='raise')
        if self.mPrintInfo==True:
            print('{:}|{:}| Finish, best params=none'.format(self.mModelName, self.mDataSet.GetDataSetName()))
        super().SaveModel(lClsModel)
        return (self.mModelName, self.mDataSet.GetDataSetName(), np.mean(lScore))
       
#==================================================================================================


#**************************************************************************************************
#Mlp model
#--------------------------------------------------------------------------------------------------
class RandIntTuple:
    def __init__(self, aMin, aMax):
        self.mRandInt=scipy.stats.randint(low=aMin, high=aMax)
    def rvs(self, *args, **kwargs):
        return (self.mRandInt.rvs(*args, **kwargs), )
        

class MlpCls(ModelBase):
    def __init__(self, aPath, aDataSet, aMinHidSize=50, aMaxHidSize=200, aMinLearnRate=0.0001, aMaxLearnRate=0.1, aMinIter=50, aMaxIter=500):
        ModelBase.__init__(self, 'Neural network classification', aPath, aDataSet)
        self.mMinHidSize=aMinHidSize
        self.mMaxHidSize=aMaxHidSize
        self.mMinLearnRate=aMinLearnRate
        self.mMaxLearnRate=aMaxLearnRate
        self.mMinEpoch=aMinIter
        self.mMaxEpoch=aMaxIter
        
    def TrainEvaluateCv(self):
        if self.mPrintInfo==True:
            print('{:}|{:}| Start, iterations={:}, score={:}'.format(self.mModelName, self.mDataSet.GetDataSetName(), self.mIterationNum, self.mScore))
        lXTrain, lYTrain=self.mDataSet.GetData('train')
        lClsModel=sklearn.neural_network.MLPClassifier(activation='relu', random_state=0)
        lClsModelPrm={'hidden_layer_sizes': RandIntTuple(self.mMinHidSize, self.mMaxHidSize), 'learning_rate_init': scipy.stats.reciprocal(self.mMinLearnRate, self.mMaxLearnRate), 'max_iter': scipy.stats.randint(low=self.mMinEpoch, high=self.mMaxEpoch)}
        lClsModelRndCv=sklearn.model_selection.RandomizedSearchCV(lClsModel, lClsModelPrm, n_iter=self.mIterationNum, scoring=self.mScore, n_jobs=-1, iid=False, cv=self.mCvGen, verbose=0, random_state=0, error_score='raise')
        lClsModelRndCv.fit(lXTrain, lYTrain)
        if self.mPrintInfo==True:
            print('{:}|{:}| Finish, best params={:}'.format(self.mModelName, self.mDataSet.GetDataSetName(), lClsModelRndCv.best_params_))
        super().SaveModel(lClsModelRndCv.best_estimator_)
        return (self.mModelName, self.mDataSet.GetDataSetName(), lClsModelRndCv.best_score_)
       
#==================================================================================================

class Action(enum.Enum):
    TRAIN=0
    TEST=1

def Usage():
    print('Commandline-options:')
    print('-a, --action\t\tAction to perform can be either \'train\' or \'test\'')
    print('-d, --datapath\t\tpath to directory with data set directories')
    print('-m, --modelpath\t\tpath to directory with trained models')
    print('-h, --help\t\tthis page')
    
def main():
    lAction=None
    lDataPath=None
    lModelPath=None
    try:
        lOpts, _= getopt.getopt(sys.argv[1:], 'ha:d:m:', ['help', 'action=', 'datapath=', 'modelpath='])
    except getopt.GetoptError as aErr:
        # print help information and exit:
        print(aErr)
        Usage()
        sys.exit(2)
    for lOption, lArgument in lOpts:
        if lOption in ('-a', '--action'):
            if lArgument=='train':
                lAction=Action.TRAIN
            elif lArgument=='test':
                lAction=Action.TEST
            else:
                Usage()
                sys.exit(2)
        elif lOption in ("-d", "--datapath"):
            lDataPath=lArgument
        elif lOption in ("-m", "--modelpath"):
            lModelPath=lArgument
        elif lOption in ("-h", "--help"):
            Usage()
            sys.exit()
        else:
            assert False, "Unknown option"
    
    global ADULT_PATH
    global YEAST_PATH
    global CREDITDEFAULT_PATH
    global THORACICSURGERY_PATH
    global SEISMICBUMPS_PATH
    global DIABETICRTY_PATH
    global STEELPLTFLT_PATH
    global GERMANCREDIT_PATH
    global AUSTRALIANCREDIT_PATH
    global BRCANWISC_PATH
    global BRCANWISCDIAG_PATH
    global BRCANWISCPROG_PATH

    #RA: Building absolute path for data-sets    
    lAllPaths=[ADULT_PATH, YEAST_PATH, CREDITDEFAULT_PATH, THORACICSURGERY_PATH, SEISMICBUMPS_PATH, DIABETICRTY_PATH, STEELPLTFLT_PATH, GERMANCREDIT_PATH, AUSTRALIANCREDIT_PATH, BRCANWISC_PATH, BRCANWISCDIAG_PATH, BRCANWISCPROG_PATH]
    for lCurrentPath in lAllPaths:
        for lDataType, lPath in lCurrentPath.items():
            lCurrentPath[lDataType]=os.path.join(lDataPath, lPath)
    
    lAllDataSets=[AdultData(ADULT_PATH), YeastData(YEAST_PATH), CreditDefaultData(CREDITDEFAULT_PATH), ThoracicSurgeryData(THORACICSURGERY_PATH), SeismicBumpsData(SEISMICBUMPS_PATH), DiabeticRtyData(DIABETICRTY_PATH), SteelPltFlt(STEELPLTFLT_PATH), GermanCredit(GERMANCREDIT_PATH), AustralianCredit(AUSTRALIANCREDIT_PATH), BrCanWisc(BRCANWISC_PATH), BrCanWiscProg(BRCANWISCPROG_PATH), BrCanWiscDiag(BRCANWISCDIAG_PATH)]
    
    print('Input data')
    print('-----')
    for lDataSet in lAllDataSets:
        lX, lY=lDataSet.GetData('train')
        print('{:} | X: {:}, y: {:}'.format(lDataSet.GetDataSetName(), lX.shape, lY.shape))
    print('=====')
    
    lKngClsAll=[KngCls(lModelPath, lCurrentDataSet) for lCurrentDataSet in lAllDataSets]
    lSvmClsAll=[SvmCls(lModelPath, lCurrentDataSet) for lCurrentDataSet in lAllDataSets]
    lDtClsAll=[DtCls(lModelPath, lCurrentDataSet) for lCurrentDataSet in lAllDataSets]
    lRfClsAll=[RfCls(lModelPath, lCurrentDataSet) for lCurrentDataSet in lAllDataSets]
    lAbClsAll=[AbCls(lModelPath, lCurrentDataSet) for lCurrentDataSet in lAllDataSets]
    lLrClsAll=[LrCls(lModelPath, lCurrentDataSet) for lCurrentDataSet in lAllDataSets]
    lGnbClsAll=[GnbCls(lModelPath, lCurrentDataSet) for lCurrentDataSet in lAllDataSets]
    lMlpClsAll=[MlpCls(lModelPath, lCurrentDataSet) for lCurrentDataSet in lAllDataSets]
    
    lAllModels=lKngClsAll+lSvmClsAll+lDtClsAll+lRfClsAll+lAbClsAll+lLrClsAll+lGnbClsAll+lMlpClsAll
    
    if lAction==Action.TRAIN:
        for lCurrentModel in lAllModels:
            lModelName, lDataName, lAccuracyScore=lCurrentModel.TrainEvaluateCv()
            print('{:}|{:}|{:.4f}'.format(lModelName, lDataName, lAccuracyScore), flush=True)
    elif lAction==Action.TEST:
        for lCurrentModel in lAllModels:
            lModelName, lDataName, lAccuracyScore=lCurrentModel.TestEvaluateCv()
            print('{:}|{:}|{:.4f}'.format(lModelName, lDataName, lAccuracyScore), flush=True)
    else:
        print('ERROR')
        sys.exit(2)

if __name__ == "__main__":
    main()
