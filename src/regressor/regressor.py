#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
import getopt
import enum
import os
import numpy as np
import sklearn
import sklearn.impute
import sklearn.neighbors
import sklearn.svm
import sklearn.tree
import sklearn.ensemble
import sklearn.gaussian_process.kernels
import sklearn.neural_network
import pandas as pd
import scipy
import pickle
import warnings
warnings.filterwarnings("ignore", category=FutureWarning)
#warnings.filterwarnings("ignore", category=ConvergenceWarning)

#RA: Defining data-set files locations relative to '--datapath' specified as command-
BIKESHARING_PATH={'train': 'Bike Sharing/hour.csv'}
COMMCRIME_PATH={'train': 'Communities and Crime/communities.data'}
CONCRETE_PATH={'train': 'Concrete Compressive Strength/Concrete_Data.xls'}
FBMTR_PATH={'train': 'Facebook metrics/dataset_Facebook.csv'}
MMAACT2_PATH={'train': 'Merck Molecular Activity/ACT2_competition_training.csv'}
MMAACT4_PATH={'train': 'Merck Molecular Activity/ACT4_competition_training.csv'}
PKSSPCH_PATH={'train': 'Parkinson Speech/train_data.txt'}
QSARAQTX_PATH={'train': 'QSAR aquatic toxicity/qsar_aquatic_toxicity.csv'}
SGEMMGPU_PATH={'train': 'SGEMM GPU kernel performance/sgemm_product.csv'}
STUDPERF_PATH={'train': 'Student Performance/student-por.csv'}
WINEQRED_PATH={'train': 'Wine Quality/winequality-red.csv'}
WINEQWHITE_PATH={'train': 'Wine Quality/winequality-white.csv'}



#**************************************************************************************************
#Data processor
#RA: Base data class - defines some generic functionality and shared data-memeners, such as name of the particular data-class
#--------------------------------------------------------------------------------------------------
class DataProcessor:
    def __init__(self, aDataSetName, aDataPath, aScoreMetric='r2', aSearchIterations=20):
        self.mDataSetName=aDataSetName
        self.mPath=aDataPath
        self.mScoreMetric=aScoreMetric
        self.mSearchIterations=aSearchIterations
        
    def GetDataSetName(self):
        return self.mDataSetName
    
    def GetScoreMetric(self):
        return self.mScoreMetric
    
    def GetSearchIterations(self):
        return self.mSearchIterations
        
    def ProcessData(self, aData, aHandling):        
        lData=aData
        if aHandling=='filter':
            lData=lData[~np.isnan(lData).any(axis=1)]
        elif aHandling=='impute':
            lImp=sklearn.impute.SimpleImputer(missing_values=np.nan, strategy='most_frequent', copy=False)
            lImp.fit(lData)
            lData=lImp.transform(lData)
        return lData
#==================================================================================================


#**************************************************************************************************
#BikeSharing data
#--------------------------------------------------------------------------------------------------
class BikeSharingData(DataProcessor):
    def __init__(self, aPath, aSuffix=''):
        DataProcessor.__init__(self, 'Bike sharing data'+aSuffix, aPath)
        
    def GetData(self, aDataType, aHandling='filter'):
        lPath=self.mPath[aDataType]
        lBikeSharingData=np.loadtxt(lPath, delimiter=',', usecols=range(2, 17), skiprows=1, encoding='UTF-8', dtype=np.float64)
        lBikeSharingData=super().ProcessData(lBikeSharingData, aHandling)
        return (lBikeSharingData[:,:-3].astype(float), lBikeSharingData[:,-1].astype(int))
#==================================================================================================        


#**************************************************************************************************
#CommCrime data
#--------------------------------------------------------------------------------------------------
class CommCrimeData(DataProcessor):
    def __init__(self, aPath, aSuffix=''):
        DataProcessor.__init__(self, 'Communities and Crime'+aSuffix, aPath)
        
    def __CommCrimeDataFloatParser(self, aValue):
        try:
            return float(aValue)
        except:
            return None        
        
    def GetData(self, aDataType, aHandling='filter'):
        lRange=(0,)+tuple((lIndex) for lIndex in range(5, 101))+tuple((lIndex) for lIndex in range(118, 121))+(127,)
        lConv={lCounter: self.__CommCrimeDataFloatParser for(lCounter) in range(100)}
        lPath=self.mPath[aDataType]
        lCommCrimeData=np.loadtxt(lPath, delimiter=',', converters=lConv, usecols=lRange, encoding='UTF-8', dtype=np.float64)
        lCommCrimeData=super().ProcessData(lCommCrimeData, aHandling)
        return (lCommCrimeData[:,:-1], lCommCrimeData[:,-1])
#==================================================================================================


#**************************************************************************************************
#ConcreteStr data
#--------------------------------------------------------------------------------------------------
class ConcreteStrData(DataProcessor):
    def __init__(self, aPath, aSuffix=''):
        DataProcessor.__init__(self, 'Concrete Compressive Strength'+aSuffix, aPath)
        
    def GetData(self, aDataType, aHandling='filter'):
        lPath=self.mPath[aDataType]
        lConcrteStrData=np.array(pd.read_excel(lPath, usecols='A:I', header=None, skiprows=1))
        lConcrteStrData=super().ProcessData(lConcrteStrData, aHandling)
        return (lConcrteStrData[:,:-1], lConcrteStrData[:,-1])
#==================================================================================================

        
#**************************************************************************************************
#ParkinsonSpch data
#--------------------------------------------------------------------------------------------------
class ParkinsonSpchData(DataProcessor):
    def __init__(self, aPath, aSuffix=''):
        DataProcessor.__init__(self, 'Parkinson speech'+aSuffix, aPath)
        
    def GetData(self, aDataType, aHandling='filter'):
        lPath=self.mPath[aDataType]
        lParkinsonSpchData=np.loadtxt(lPath, delimiter=',', encoding='UTF-8', dtype=np.float64)
        lParkinsonSpchData=super().ProcessData(lParkinsonSpchData, aHandling)
        return (lParkinsonSpchData[:,1:27], lParkinsonSpchData[:,-2].astype(int))
#==================================================================================================
        
    
#**************************************************************************************************
#QsarAqTox data
#--------------------------------------------------------------------------------------------------
class QsarAqToxData(DataProcessor):
    def __init__(self, aPath, aSuffix=''):
        DataProcessor.__init__(self, 'QSAR aquatic toxicity'+aSuffix, aPath)
        
    def GetData(self, aDataType, aHandling='filter'):
        lPath=self.mPath[aDataType]
        lQsarAqToxData=np.loadtxt(lPath, delimiter=';', encoding='UTF-8', dtype=np.float64)
        lQsarAqToxData=super().ProcessData(lQsarAqToxData, aHandling)
        return (lQsarAqToxData[:,:-1], lQsarAqToxData[:,-1])
#==================================================================================================    


#**************************************************************************************************
#SgemmGpu data
#--------------------------------------------------------------------------------------------------
class SgemmGpuData(DataProcessor):
    def __init__(self, aPath, aSuffix=''):
        DataProcessor.__init__(self, 'SGEMM GPU kernel performance'+aSuffix, aPath, aSearchIterations=10)
        
    def GetData(self, aDataType, aHandling='filter'):
        lPath=self.mPath[aDataType]
        lSgemmGpuData=np.loadtxt(lPath, delimiter=',', skiprows=1, encoding='UTF-8', dtype=np.float64)
        lSgemmGpuData=super().ProcessData(lSgemmGpuData, aHandling)
        return (lSgemmGpuData[:,:-4], np.mean(lSgemmGpuData[:,-4:], axis=1))
#==================================================================================================
        
    
#**************************************************************************************************
#StudPerf data
#--------------------------------------------------------------------------------------------------
class StudPerfData(DataProcessor):
    def __init__(self, aPath, aSuffix=''):
        DataProcessor.__init__(self, 'Student Performance'+aSuffix, aPath)
        
    def __StudPerfSchoolParser(self, aValue):
        lValueMapper={'GP': 0, 'MS': 1}
        return lValueMapper.get(aValue.strip('\"'))        
    
    def __StudPerfSexParser(self, aValue):
        lValueMapper={'M': 0, 'F': 1}
        return lValueMapper.get(aValue.strip('\"'))
    
    def __StudPerfIntParser(self, aValue):
        try:
            return int(aValue.strip('\"'))
        except:
            return None
        
    def __StudPerfAddrParser(self, aValue):
        lValueMapper={'U': 0, 'R': 1}
        return lValueMapper.get(aValue.strip('\"')) 
    
    def __StudPerfFamSizeParser(self, aValue):
        lValueMapper={'LE3': 0, 'GT3': 1}
        return lValueMapper.get(aValue.strip('\"'))
    
    def __StudPerfParStatParser(self, aValue):
        lValueMapper={'T': 0, 'A': 1}
        return lValueMapper.get(aValue.strip('\"'))
    
    def __StudPerfParJobParser(self, aValue):
        lValueMapper={'teacher': 0, 'health': 1, 'services': 2, 'at_home': 3, 'other': 4}
        return lValueMapper.get(aValue.strip('\"'))
    
    def __StudPerfReasonParser(self, aValue):
        lValueMapper={'home': 0, 'reputation': 1, 'course': 2, 'other': 3}
        return lValueMapper.get(aValue.strip('\"'))
    
    def __StudPerfGuardParser(self, aValue):
        lValueMapper={'mother': 0, 'father': 1, 'other': 2}
        return lValueMapper.get(aValue.strip('\"'))
    
    def __StudPerfYesNoParser(self, aValue):
        lValueMapper={'no': 0, 'yes': 1}
        return lValueMapper.get(aValue.strip('\"'))
        
    def GetData(self, aDataType, aHandling='filter'):
        lConv={0: self.__StudPerfSchoolParser, 1: self.__StudPerfSexParser, 2: self.__StudPerfIntParser, 3: self.__StudPerfAddrParser, 4: self.__StudPerfFamSizeParser, 5: self.__StudPerfParStatParser, 6: self.__StudPerfIntParser, 7: self.__StudPerfIntParser, 8: self.__StudPerfParJobParser, 9: self.__StudPerfParJobParser, 10: self.__StudPerfReasonParser, 11: self.__StudPerfGuardParser, 12:self.__StudPerfIntParser, 13: self.__StudPerfIntParser, 14: self.__StudPerfIntParser, 15: self.__StudPerfYesNoParser, 16: self.__StudPerfYesNoParser, 17: self.__StudPerfYesNoParser, 18: self.__StudPerfYesNoParser, 19: self.__StudPerfYesNoParser, 20: self.__StudPerfYesNoParser, 21: self.__StudPerfYesNoParser, 22: self.__StudPerfYesNoParser, 23: self.__StudPerfIntParser, 24: self.__StudPerfIntParser, 25: self.__StudPerfIntParser, 26: self.__StudPerfIntParser, 27: self.__StudPerfIntParser, 28: self.__StudPerfIntParser, 29: self.__StudPerfIntParser, 30: self.__StudPerfIntParser, 31: self.__StudPerfIntParser, 32: self.__StudPerfIntParser}
        lPath=self.mPath[aDataType]
        lStudPerfData=np.loadtxt(lPath, delimiter=';', converters=lConv, skiprows=1, encoding='UTF-8', dtype=np.float64)
        lStudPerfData=super().ProcessData(lStudPerfData, aHandling)
        return (lStudPerfData[:,:-3].astype(int), lStudPerfData[:,-1].astype(int))
#==================================================================================================    
        

#**************************************************************************************************
#WineQuality data
#--------------------------------------------------------------------------------------------------
class WineQualityData(DataProcessor):
    def __init__(self, aPath, aSuffix=''):
        DataProcessor.__init__(self, 'Wine quality'+aSuffix, aPath)
        
    def GetData(self, aDataType, aHandling='filter'):
        lPath=self.mPath[aDataType]
        lWineQualityData=np.loadtxt(lPath, delimiter=';', skiprows=1, encoding='UTF-8', dtype=np.float64)
        lWineQualityData=super().ProcessData(lWineQualityData, aHandling)
        return (lWineQualityData[:,:-1], lWineQualityData[:,-1].astype(int))
#==================================================================================================

        
#**************************************************************************************************
#MerckMolAct data
#--------------------------------------------------------------------------------------------------
class MerckMolAct(DataProcessor):
    def __init__(self, aPath, aSuffix=''):
        DataProcessor.__init__(self, 'Merck Molecular Activity'+aSuffix, aPath, aSearchIterations=10)
        
    def GetData(self, aDataType, aHandling='filter'):        
        lPath=self.mPath[aDataType]
        with open(lPath) as lFile:
            lColumns=lFile.readline().rstrip('\n').split(',')
        lMerckMolActX=np.loadtxt(lPath, delimiter=',', usecols=range(2, len(lColumns)), skiprows=1, dtype=np.uint8)
        lMerckMolActY=np.loadtxt(lPath, delimiter=',', usecols=1, skiprows=1)
        return (lMerckMolActX, lMerckMolActY)
#==================================================================================================    


#**************************************************************************************************
#FbMetData data
#--------------------------------------------------------------------------------------------------
class FbMetData(DataProcessor):
    def __init__(self, aPath, aSuffix=''):
        DataProcessor.__init__(self, 'Facebook metrics'+aSuffix, aPath)
        
    def __FbMetDataIntParser(self, aValue):
        try:
            return int(aValue)
        except:
            return None
        
    def __FbMetDataTypeParser(self, aValue):
        lValueMapper={'Link': 0, 'Photo': 1, 'Video': 2, 'Status': 3}
        return lValueMapper.get(aValue)
        
    def GetData(self, aDataType, aHandling='filter'):
        lConv={0: self.__FbMetDataIntParser, 1: self.__FbMetDataTypeParser, 2: self.__FbMetDataIntParser, 3: self.__FbMetDataIntParser, 4: self.__FbMetDataIntParser, 5: self.__FbMetDataIntParser, 6: self.__FbMetDataIntParser, 7: self.__FbMetDataIntParser, 8: self.__FbMetDataIntParser, 9: self.__FbMetDataIntParser, 10: self.__FbMetDataIntParser, 11: self.__FbMetDataIntParser, 12: self.__FbMetDataIntParser, 13: self.__FbMetDataIntParser, 14: self.__FbMetDataIntParser, 15: self.__FbMetDataIntParser, 16: self.__FbMetDataIntParser, 17: self.__FbMetDataIntParser, 18: self.__FbMetDataIntParser}
        lPath=self.mPath[aDataType]
        lFbMetData=np.loadtxt(lPath, delimiter=';', skiprows=1, encoding='UTF-8', converters=lConv)
        lFbMetData=super().ProcessData(lFbMetData, aHandling)
        return (lFbMetData[:,:7].astype(int), lFbMetData[:,-3].astype(int))
#==================================================================================================


#**************************************************************************************************
#ModelBase
#--------------------------------------------------------------------------------------------------
class ModelBase:
    def __init__(self, aModelName, aModelPath, aDataSet):
        self.mModelName=aModelName
        self.mPath=aModelPath
        self.mDataSet=aDataSet
        self.mIterationNum=aDataSet.GetSearchIterations()
        self.mScore=aDataSet.GetScoreMetric()
        self.mCvGen=sklearn.model_selection.KFold(n_splits=10, random_state=0)
        self.mPrintInfo=True
        
    def GetModelName(self):
        return self.mModelName
    
    def __BuildModelPath(self):
        return os.path.join(self.mPath, self.mModelName+'_'+self.mDataSet.GetDataSetName()+'.model')
    
    def SaveModel(self, aModel):
        if not os.path.exists(self.mPath):
            os.makedirs(self.mPath)
        lModelFile=self.__BuildModelPath()
        pickle.dump(aModel, open(lModelFile, 'wb'), protocol=4)
        
    def LoadModel(self):
        lModelFile=self.__BuildModelPath()
        return pickle.load(open(lModelFile, 'rb'))
    
    def TestEvaluateCv(self):
        lX, lY=self.mDataSet.GetData('train')
        lRgsModel=self.LoadModel()
        lScore=sklearn.model_selection.cross_val_score(lRgsModel, lX, lY, scoring=self.mScore, cv=self.mCvGen, n_jobs=-1, verbose=0, error_score='raise')
        return (self.mModelName, self.mDataSet.GetDataSetName(), np.mean(lScore))
#==================================================================================================
        

#**************************************************************************************************
#SVM model
#--------------------------------------------------------------------------------------------------
class SvmRgs(ModelBase):
    def __init__(self, aPath, aDataSet, aMinRcpC=1, aMaxRcpC=1000, aMinRcpGamma=0.001, aMaxRcpGamma=10):
        ModelBase.__init__(self, 'SVM Regressor', aPath, aDataSet)        
        self.mMinC=aMinRcpC
        self.mMaxC=aMaxRcpC
        self.mMinGamma=aMinRcpGamma
        self.mMaxGamma=aMaxRcpGamma
        
    def TrainEvaluateCv(self):
        if self.mPrintInfo==True:
            print('{:}|{:}| Start, iterations={:}, score={:}'.format(self.mModelName, self.mDataSet.GetDataSetName(), self.mIterationNum, self.mScore))        
        lXTrain, lYTrain=self.mDataSet.GetData('train')
        lRgsModel=sklearn.svm.SVR(kernel='rbf')
        lRgsModelPrm={'C': scipy.stats.reciprocal(self.mMinC, self.mMaxC), 'gamma': scipy.stats.reciprocal(self.mMinGamma, self.mMaxGamma)}
        lRgsModelRndCv=sklearn.model_selection.RandomizedSearchCV(lRgsModel, lRgsModelPrm, n_iter=self.mIterationNum, scoring=self.mScore, n_jobs=-1, iid=False, cv=self.mCvGen, verbose=0, random_state=0, error_score='raise')
        lRgsModelRndCv.fit(lXTrain, lYTrain)
        if self.mPrintInfo==True:
            print('{:}|{:}| Finish, best params={:}'.format(self.mModelName, self.mDataSet.GetDataSetName(), lRgsModelRndCv.best_params_))
        super().SaveModel(lRgsModelRndCv.best_estimator_)
        return (self.mModelName, self.mDataSet.GetDataSetName(), lRgsModelRndCv.best_score_)
       
#==================================================================================================


#**************************************************************************************************
#DTR model
#--------------------------------------------------------------------------------------------------
class DtrRgs(ModelBase):
    def __init__(self, aPath, aDataSet, aMinMaxDepth=10, aMaxMaxDepth=300):
        ModelBase.__init__(self, 'Decision Tree Regressor', aPath, aDataSet)        
        self.mMinMaxDepth=aMinMaxDepth
        self.mMaxMaxDepth=aMaxMaxDepth
        
    def TrainEvaluateCv(self):
        if self.mPrintInfo==True:
            print('{:}|{:}| Start, iterations={:}, score={:}'.format(self.mModelName, self.mDataSet.GetDataSetName(), self.mIterationNum, self.mScore))
        lXTrain, lYTrain=self.mDataSet.GetData('train')
        lRgsModel=sklearn.tree.DecisionTreeRegressor(random_state=0)
        lRgsModelPrm={'max_depth': scipy.stats.randint(low=self.mMinMaxDepth, high=self.mMaxMaxDepth)}
        lRgsModelRndCv=sklearn.model_selection.RandomizedSearchCV(lRgsModel, lRgsModelPrm, n_iter=self.mIterationNum, scoring=self.mScore, n_jobs=-1, iid=False, cv=self.mCvGen, verbose=0, random_state=0, error_score='raise')
        lRgsModelRndCv.fit(lXTrain, lYTrain)
        if self.mPrintInfo==True:
            print('{:}|{:}| Finish, best params={:}'.format(self.mModelName, self.mDataSet.GetDataSetName(), lRgsModelRndCv.best_params_))
        super().SaveModel(lRgsModelRndCv.best_estimator_)
        return (self.mModelName, self.mDataSet.GetDataSetName(), lRgsModelRndCv.best_score_)
       
#==================================================================================================


#**************************************************************************************************
#RFR model
#--------------------------------------------------------------------------------------------------
class RfrRgs(ModelBase):
    def __init__(self, aPath, aDataSet, aMinEstimators=10, aMaxEstimators=200, aMinMaxDepth=10, aMaxMaxDepth=300):
        ModelBase.__init__(self, 'Random Forest Regressor', aPath, aDataSet)        
        self.mMinEstimators=aMinEstimators
        self.mMaxEstimators=aMaxEstimators
        self.mMinMaxDepth=aMinMaxDepth
        self.mMaxMaxDepth=aMaxMaxDepth
        
    def TrainEvaluateCv(self):
        if self.mPrintInfo==True:
            print('{:}|{:}| Start, iterations={:}, score={:}'.format(self.mModelName, self.mDataSet.GetDataSetName(), self.mIterationNum, self.mScore))
        lXTrain, lYTrain=self.mDataSet.GetData('train')
        lRgsModel=sklearn.ensemble.RandomForestRegressor(random_state=0)
        lRgsModelPrm={'n_estimators': scipy.stats.randint(low=self.mMinEstimators, high=self.mMaxEstimators), 'max_depth': scipy.stats.randint(low=self.mMinMaxDepth, high=self.mMaxMaxDepth)}
        lRgsModelRndCv=sklearn.model_selection.RandomizedSearchCV(lRgsModel, lRgsModelPrm, n_iter=self.mIterationNum, scoring=self.mScore, n_jobs=-1, iid=False, cv=self.mCvGen, verbose=0, random_state=0, error_score='raise')
        lRgsModelRndCv.fit(lXTrain, lYTrain)
        if self.mPrintInfo==True:
            print('{:}|{:}| Finish, best params={:}'.format(self.mModelName, self.mDataSet.GetDataSetName(), lRgsModelRndCv.best_params_))
        super().SaveModel(lRgsModelRndCv.best_estimator_)
        return (self.mModelName, self.mDataSet.GetDataSetName(), lRgsModelRndCv.best_score_)
       
#==================================================================================================


#**************************************************************************************************
#ABR model
#--------------------------------------------------------------------------------------------------
class AbrRgs(ModelBase):
    def __init__(self, aPath, aDataSet, aMinEstimators=10, aMaxEstimators=200):
        ModelBase.__init__(self, 'Ada Boost Regressor', aPath, aDataSet)        
        self.mMinEstimators=aMinEstimators
        self.mMaxEstimators=aMaxEstimators
        
    def TrainEvaluateCv(self):
        if self.mPrintInfo==True:
            print('{:}|{:}| Start, iterations={:}, score={:}'.format(self.mModelName, self.mDataSet.GetDataSetName(), self.mIterationNum, self.mScore))
        lXTrain, lYTrain=self.mDataSet.GetData('train')
        lRgsModel=sklearn.ensemble.AdaBoostRegressor(random_state=0)
        lRgsModelPrm={'n_estimators': scipy.stats.randint(low=self.mMinEstimators, high=self.mMaxEstimators)}
        lRgsModelRndCv=sklearn.model_selection.RandomizedSearchCV(lRgsModel, lRgsModelPrm, n_iter=self.mIterationNum, scoring=self.mScore, n_jobs=-1, iid=False, cv=self.mCvGen, verbose=0, random_state=0, error_score='raise')
        lRgsModelRndCv.fit(lXTrain, lYTrain)
        if self.mPrintInfo==True:
            print('{:}|{:}| Finish, best params={:}'.format(self.mModelName, self.mDataSet.GetDataSetName(), lRgsModelRndCv.best_params_))
        super().SaveModel(lRgsModelRndCv.best_estimator_)
        return (self.mModelName, self.mDataSet.GetDataSetName(), lRgsModelRndCv.best_score_)
       
#==================================================================================================


#**************************************************************************************************
#GsRgs model
#--------------------------------------------------------------------------------------------------
class GsRgs(ModelBase):
    def __init__(self, aPath, aDataSet, aMinAlpha=1e-10, aMaxAlpha=1e-2):
        ModelBase.__init__(self, 'Gaussian Process Regressor', aPath, aDataSet)
        self.mMinAlpha=aMinAlpha
        self.mMaxAlpha=aMaxAlpha
        
    def TrainEvaluateCv(self):
        if self.mPrintInfo==True:
            print('{:}|{:}| Start, iterations={:}, score={:}'.format(self.mModelName, self.mDataSet.GetDataSetName(), self.mIterationNum, self.mScore))
        lXTrain, lYTrain=self.mDataSet.GetData('train')
#RA: One of the possible kernel definitions, though default is used in project        
#        lKernel=sklearn.gaussian_process.kernels.ConstantKernel()*sklearn.gaussian_process.kernels.RBF()
        lRgsModel=sklearn.gaussian_process.GaussianProcessRegressor(random_state=0)
        lRgsModelPrm={'alpha': scipy.stats.reciprocal(self.mMinAlpha, self.mMaxAlpha)}
        lRgsModelRndCv=sklearn.model_selection.RandomizedSearchCV(lRgsModel, lRgsModelPrm, n_iter=self.mIterationNum, scoring=self.mScore, n_jobs=None, iid=False, cv=self.mCvGen, verbose=0, random_state=0, error_score='raise')
        lRgsModelRndCv.fit(lXTrain, lYTrain)
        if self.mPrintInfo==True:
            print('{:}|{:}| Finish, best params={:}'.format(self.mModelName, self.mDataSet.GetDataSetName(), lRgsModelRndCv.best_params_))
        super().SaveModel(lRgsModelRndCv.best_estimator_)
        return (self.mModelName, self.mDataSet.GetDataSetName(), lRgsModelRndCv.best_score_)
       
#==================================================================================================


#**************************************************************************************************
#LinRgs model
#--------------------------------------------------------------------------------------------------
class LinRgs(ModelBase):
    def __init__(self, aPath, aDataSet):
        ModelBase.__init__(self, 'Linear Regression', aPath, aDataSet)        
        
    def TrainEvaluateCv(self):
        if self.mPrintInfo==True:
            print('{:}|{:}| Start, iterations={:}, score={:}'.format(self.mModelName, self.mDataSet.GetDataSetName(), self.mIterationNum, self.mScore))
        lXTrain, lYTrain=self.mDataSet.GetData('train')
        lRgsModel=sklearn.linear_model.LinearRegression(fit_intercept=True, normalize=True, n_jobs=-1)
        lScore=sklearn.model_selection.cross_val_score(lRgsModel, lXTrain, lYTrain, scoring=self.mScore, cv=self.mCvGen, n_jobs=-1, verbose=0, error_score='raise')
        if self.mPrintInfo==True:
            print('{:}|{:}| Finish, best params=none'.format(self.mModelName, self.mDataSet.GetDataSetName()))
        super().SaveModel(lRgsModel)
        return (self.mModelName, self.mDataSet.GetDataSetName(), np.mean(lScore))
       
#==================================================================================================


#**************************************************************************************************
#MlpRgs model
#--------------------------------------------------------------------------------------------------
class RandIntTuple:
    def __init__(self, aMin, aMax):
        self.mRandInt=scipy.stats.randint(low=aMin, high=aMax)
    def rvs(self, *args, **kwargs):
        return (self.mRandInt.rvs(*args, **kwargs), )
        

class MlpRgs(ModelBase):
    def __init__(self, aPath, aDataSet, aMinHidSize=50, aMaxHidSize=1000, aMinLearnRate=0.0001, aMaxLearnRate=0.1, aMinIter=10, aMaxIter=200):
        ModelBase.__init__(self, 'Neural network regression', aPath, aDataSet)
        self.mMinHidSize=aMinHidSize
        self.mMaxHidSize=aMaxHidSize
        self.mMinLearnRate=aMinLearnRate
        self.mMaxLearnRate=aMaxLearnRate
        self.mMinEpoch=aMinIter
        self.mMaxEpoch=aMaxIter
        
    def TrainEvaluateCv(self):
        if self.mPrintInfo==True:
            print('{:}|{:}| Start, iterations={:}, score={:}'.format(self.mModelName, self.mDataSet.GetDataSetName(), self.mIterationNum, self.mScore))
        lXTrain, lYTrain=self.mDataSet.GetData('train')
        lRgsModel=sklearn.neural_network.MLPRegressor(activation='relu', random_state=0)
        lRgsModelPrm={'hidden_layer_sizes': RandIntTuple(self.mMinHidSize, self.mMaxHidSize), 'learning_rate_init': scipy.stats.reciprocal(self.mMinLearnRate, self.mMaxLearnRate), 'max_iter': scipy.stats.randint(low=self.mMinEpoch, high=self.mMaxEpoch)}
        lRgsModelRndCv=sklearn.model_selection.RandomizedSearchCV(lRgsModel, lRgsModelPrm, n_iter=self.mIterationNum, scoring=self.mScore, n_jobs=-1, iid=False, cv=self.mCvGen, verbose=0, random_state=0, error_score='raise')
        lRgsModelRndCv.fit(lXTrain, lYTrain)
        if self.mPrintInfo==True:
            print('{:}|{:}| Finish, best params={:}'.format(self.mModelName, self.mDataSet.GetDataSetName(), lRgsModelRndCv.best_params_))
        super().SaveModel(lRgsModelRndCv.best_estimator_)
        return (self.mModelName, self.mDataSet.GetDataSetName(), lRgsModelRndCv.best_score_)
       
#==================================================================================================



class Action(enum.Enum):
    TRAIN=0
    TEST=1

def Usage():
    print('Commandline-options:')
    print('-a, --action\t\tAction to perform can be either \'train\' or \'test\'')
    print('-d, --datapath\t\tpath to directory with data set directories')
    print('-m, --modelpath\t\tpath to directory with trained models')
    print('-h, --help\t\tthis page')
    
def main():
    lAction=None
    lDataPath=None
    lModelPath=None
#RA: Initializing seed used by default in certain sklearn functions
    lSeed=np.random.seed(0)
    
#RA: Comman-line processor    
    try:
        lOpts, _= getopt.getopt(sys.argv[1:], 'ha:d:m:', ['help', 'action=', 'datapath=', 'modelpath='])
    except getopt.GetoptError as aErr:
        print(aErr)
        Usage()
        sys.exit(2)
    for lOption, lArgument in lOpts:
        if lOption in ('-a', '--action'):
            if lArgument=='train':
                lAction=Action.TRAIN
            elif lArgument=='test':
                lAction=Action.TEST
            else:
                Usage()
                sys.exit(2)
        elif lOption in ("-d", "--datapath"):
            lDataPath=lArgument
        elif lOption in ("-m", "--modelpath"):
            lModelPath=lArgument
        elif lOption in ("-h", "--help"):
            Usage()
            sys.exit()
        else:
            assert False, "Unknown option"
    
    global BIKESHARING_PATH
    global COMMCRIME_PATH
    global CONCRETE_PATH
    global FBMTR_PATH
    global MMAACT2_PATH
    global MMAACT4_PATH
    global PKSSPCH_PATH
    global QSARAQTX_PATH
    global SGEMMGPU_PATH
    global STUDPERF_PATH
    global WINEQRED_PATH
    global WINEQWHITE_PATH

#RA: Building absolute path for data-sets    
    lAllPaths=[BIKESHARING_PATH, COMMCRIME_PATH, CONCRETE_PATH, FBMTR_PATH, MMAACT2_PATH, MMAACT4_PATH, PKSSPCH_PATH, QSARAQTX_PATH, SGEMMGPU_PATH, STUDPERF_PATH, WINEQRED_PATH, WINEQWHITE_PATH]
    for lCurrentPath in lAllPaths:
        for lDataType, lPath in lCurrentPath.items():
            lCurrentPath[lDataType]=os.path.join(lDataPath, lPath)

#RA: Array of all data-set classes    
    lAllDataSets=[BikeSharingData(BIKESHARING_PATH), CommCrimeData(COMMCRIME_PATH), ConcreteStrData(CONCRETE_PATH), ParkinsonSpchData(PKSSPCH_PATH), QsarAqToxData(QSARAQTX_PATH), SgemmGpuData(SGEMMGPU_PATH), StudPerfData(STUDPERF_PATH), WineQualityData(WINEQRED_PATH, aSuffix='_red'), WineQualityData(WINEQWHITE_PATH, aSuffix='_white'), MerckMolAct(MMAACT2_PATH, aSuffix='_2'), MerckMolAct(MMAACT4_PATH, aSuffix='_4'), FbMetData(FBMTR_PATH)]
    
    print('Input data')
    print('-----')
    for lDataSet in lAllDataSets:
        lX, lY=lDataSet.GetData('train')
        print('{:} | X: {:}, y: {:}'.format(lDataSet.GetDataSetName(), lX.shape, lY.shape))
    print('=====')
    
#RA: Initialize model classes agains all evaluated data-sets   
    lSvmRgsAll=[SvmRgs(lModelPath, lCurrentDataSet) for lCurrentDataSet in lAllDataSets]
    lDtrRgsAll=[DtrRgs(lModelPath, lCurrentDataSet) for lCurrentDataSet in lAllDataSets]
    lRfrRgsAll=[RfrRgs(lModelPath, lCurrentDataSet) for lCurrentDataSet in lAllDataSets]
    lAbrRgsAll=[AbrRgs(lModelPath, lCurrentDataSet) for lCurrentDataSet in lAllDataSets]
    lGsRgsAll=[GsRgs(lModelPath, lCurrentDataSet) for lCurrentDataSet in lAllDataSets]
    lLinRgsAll=[LinRgs(lModelPath, lCurrentDataSet) for lCurrentDataSet in lAllDataSets]
    lMlpRgsAll=[MlpRgs(lModelPath, lCurrentDataSet) for lCurrentDataSet in lAllDataSets]
    
#RA: Array of all initalized data models    
    lAllModels=lSvmRgsAll+lDtrRgsAll+lRfrRgsAll+lAbrRgsAll+lGsRgsAll+lLinRgsAll+lMlpRgsAll
    
    if lAction==Action.TRAIN:
        for lCurrentModel in lAllModels:
            lModelName, lDataName, lAccuracyScore=lCurrentModel.TrainEvaluateCv()
            print('{:}|{:}|{:.4f}'.format(lModelName, lDataName, lAccuracyScore), flush=True)
    elif lAction==Action.TEST:
        for lCurrentModel in lAllModels:
            lModelName, lDataName, lAccuracyScore=lCurrentModel.TestEvaluateCv()
            print('{:}|{:}|{:.4f}'.format(lModelName, lDataName, lAccuracyScore), flush=True)
    else:
        print('ERROR')
        sys.exit(2)

if __name__ == "__main__":
    main()
